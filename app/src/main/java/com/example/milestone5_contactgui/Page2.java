package com.example.milestone5_contactgui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class Page2 extends AppCompatActivity {

    //Button btn_testContact;

    ListView lv_contactArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);

        //btn_testContact = findViewById(R.id.btn_testContact);
        /*
        btn_testContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToPg3 = new Intent(v.getContext(), page3.class);
                startActivity(goToPg3);
            }
        });
        */
        lv_contactArray = findViewById(R.id.lv_contactArray);

        String[] names = {"Mason", "John", "Amy", "Keri", "Bill"};

        ListAdapter myAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);

        lv_contactArray.setAdapter(myAdapter);

        lv_contactArray.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent goToPg3 = new Intent(view.getContext(), page3.class);
                        startActivity(goToPg3);
                    }
                }
        );
    }
}
