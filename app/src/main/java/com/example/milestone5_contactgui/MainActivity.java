package com.example.milestone5_contactgui;

import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btn_page2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_page2 = findViewById(R.id.btn_page2);

btn_page2.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent goToPg2 = new Intent(v.getContext(), Page2.class);
        startActivity(goToPg2);
    }
});


    }
}
