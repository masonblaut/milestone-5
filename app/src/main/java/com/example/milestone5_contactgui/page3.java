package com.example.milestone5_contactgui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class page3 extends AppCompatActivity {

    Button btn_exitContact, btn_editContact, btn_picture, btn_call, btn_directions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page3);

        btn_editContact = findViewById(R.id.btn_editContact);
        btn_exitContact = findViewById(R.id.btn_exitContact);
        btn_picture = findViewById(R.id.btn_picture);
        btn_call = findViewById(R.id.btn_call);
        btn_directions = findViewById(R.id.btn_directions);

        btn_editContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToEditContact = new Intent(v.getContext(), edit_contact.class);
                startActivity(goToEditContact);
            }
        });

        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToCall = new Intent(v.getContext(), call_app.class);
                startActivity(goToCall);
            }
        });

        btn_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToPicture = new Intent(v.getContext(), picture_app.class);
                startActivity(goToPicture);
            }
        });

        btn_exitContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToPg2 = new Intent(v.getContext(), Page2.class);
                startActivity(goToPg2);
            }
        });

        btn_directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToDirections = new Intent(v.getContext(), directions_app.class);
                startActivity(goToDirections);
            }
        });
    }
}
