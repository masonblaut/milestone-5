package com.example.milestone5_contactgui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class edit_contact extends AppCompatActivity {

    Button btn_exitEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);

        btn_exitEdit = findViewById(R.id.btn_exitEdit);

        btn_exitEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToPg3 = new Intent(v.getContext(), page3.class);
                startActivity(goToPg3);
            }
        });

    }
}
